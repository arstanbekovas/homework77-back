const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');

const config = require('../config');

const storage = multer.diskStorage(
  {
    destination: (req, file, cb) => {
      cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
      cb(null, nanoid() + path.extname(file.originalname))
    }
  }
);

const upload = multer({storage});

const router = express.Router();

const createRouter = (db) => {
  // message index
  router.get('/', (req, res) => {
    res.send(db.getData());
  });

  // message create
  router.post('/',upload.single('image'), (req, res) => {
    const message = req.body;

    if(!message.author) {
      message.author = 'anonymus'
    }

    if (req.file) {
      message.image = req.file.filename;
    } else {
      message.image = null;
    }

    db.addItem(message).then(result => {
      res.send(result);
    });
  });

  // Product get by ID
  router.get('/:id', (req, res) => {
    res.send(db.getDataById(req.params.id));
  });

  return router;
};

module.exports = createRouter;